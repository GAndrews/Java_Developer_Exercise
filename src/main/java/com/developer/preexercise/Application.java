/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.preexercise;

import java.util.HashMap;

public class Application {
    public void main(String[] args) {
        Basket basket = new Basket();
        for(String orderItem : args) {
            basket.addItem(orderItem.toLowerCase());
        }
        String.format("Total price: %s", basket.totalCost());
    }
    
    
}
