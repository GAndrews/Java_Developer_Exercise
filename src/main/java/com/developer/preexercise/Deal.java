/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.preexercise;

import java.util.ArrayList;

/**
 *
 * @author developer
 */
public enum Deal {
    appleDeal(Item.apple, 2, 1), orangeDeal(Item.orange, 3, 1);
    private Item item;
    private int quantity;
    private int discountedItems;

    private Deal(Item item, int quantity, int chargedAmount) {
        this.item = item;
        this.quantity = quantity;
        this.discountedItems = chargedAmount;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getChargedAmount() {
        return discountedItems;
    }

    public void setChargedAmount(int chargedAmount) {
        this.discountedItems = chargedAmount;
    }
    
    
}
