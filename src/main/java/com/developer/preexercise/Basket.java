/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.preexercise;

import java.util.HashMap;

    class Basket {
        HashMap<String, ItemOrder> contents = new HashMap<String, ItemOrder>();
        public void addItem(String itemName) {
            try {
                Item item = Item.valueOf(itemName);
                ItemOrder order;

                if(contents.containsKey(item.getName())) {
                    order = contents.get(item.getName());
                    order.addItem();
                } else {
                    order = new ItemOrder(item);
                    contents.put(item.getName(), order);
                }
                contents.put(item.getName(), order); 
            } catch (Exception ex) {
                System.out.println("Unable to process request for " + itemName);
            }
        }
        
        public double totalCost() {
            double total = 0;
            calculateDiscounts();
            for(ItemOrder order : contents.values()) {
                total += order.calculateCost();
            }
            return total;
        }
        
        public void calculateDiscounts() {
            for( Deal deal : Deal.values()) {
                if(contents.containsKey(deal.getItem().getName())) {
                    ItemOrder order = contents.get(deal.getItem().getName());
                    for( int i = order.getCount(); i >= deal.getQuantity(); i -= deal.getQuantity()) {
                        order.decreaseChargedAmount(deal.getChargedAmount());
                    }
                }
            }
        }
    }
