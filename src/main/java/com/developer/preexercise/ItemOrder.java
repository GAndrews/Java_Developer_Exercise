/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.preexercise;
class ItemOrder {
        private Item item;
        private int count;
        private int chargedAmount;

        public int getChargedAmount() {
            return chargedAmount;
        }

        public void setChargedAmount(int chargedAmount) {
            this.chargedAmount = chargedAmount;
        }
        
        public void decreaseChargedAmount(int chargedAmount) {
            this.chargedAmount -= chargedAmount;
        }
        
        public void addItem() {
            chargedAmount++;
            count++;
        }
        
        public Item getItem() {
            return item;
        }

        public void setItem(Item item) {
            this.item = item;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public ItemOrder(Item item) {
            this.item = item;
            this.count = 1;
            this.chargedAmount = 1;
        }
        
        public double calculateCost() {
            return chargedAmount * item.getPrice();
        }    
    }
