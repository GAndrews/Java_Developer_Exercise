/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.preexercise;

public enum Item {
    apple("Apple", 0.6), orange("Orange", 0.25);
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    private double price;
    private Item(String name, double price) {
        this.name = name;
        this.price = price;
    }
}
