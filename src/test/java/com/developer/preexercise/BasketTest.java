/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developer.preexercise;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class BasketTest {

    Basket basket;
    private void setup(String... names) {
        basket = new Basket();
        for( String itemName : names) {
            basket.addItem(itemName);
        }
    }
    @Test
    public void testApple() {
        setup("apple");
        assertTrue("The basket does not contain apples as expected",basket.contents.containsKey(Item.apple.getName()));
        assertEquals("Basket cost is not as expected",basket.totalCost(), Item.apple.getPrice(), 0);
        assertEquals("Basket size is not as expected",basket.contents.values().size(), 1);
    }
    
    @Test
    public void testOrange() {
       setup("orange");
        assertTrue("The basket does not contain oranges as expected",basket.contents.containsKey(Item.orange.getName()));
        assertEquals("Basket cost is not as expected",basket.totalCost(), Item.orange.getPrice(), 0);
        assertEquals("Basket size is not as expected",basket.contents.values().size(), 1);
    }
    
    @Test
    public void testApplesAndOranges() {
        setup("orange", "apple", "apple");
        assertTrue("The basket does not contain oranges as expected",basket.contents.containsKey(Item.orange.getName()));
        assertTrue("The basket does not contain apple as expected",basket.contents.containsKey(Item.apple.getName()));
        assertEquals("Basket cost is not as expected",basket.totalCost(), Item.orange.getPrice() + Item.apple.getPrice(), 0);
        assertEquals("Basket size is not as expected",basket.contents.values().size(), 2);
    }
    
    @Test
    public void testAppleDiscount() {
        setup("apple", "apple");
        assertEquals("The basket does not contain two apples", basket.contents.get(Item.apple.getName()).getCount(), 2);
        assertEquals("The total value doesn't not take into account the discount", basket.totalCost(), Item.apple.getPrice(), 0);
    }
    
    @Test
    public void testOrangeDiscount() {
        setup("orange", "orange", "orange");
        assertEquals("The basket doesn't contain three oranges", basket.contents.get(Item.orange.getName()).getCount(), 3);
        assertEquals(
                "The total value doesn't take into account the discount",
                basket.totalCost(),
                Item.orange.getPrice()*2, 0);
    }
}
